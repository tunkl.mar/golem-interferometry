# GOLEM interferometry

Microwave interferometry on GOLEM tokamak is not always reliable.
This repo contains new jupyter notebook for interferometry data processing, repair, and comparison with previous solution.

## Data mining

Comparison of reliability of old (pre [#31150](http://golem.fjfi.cvut.cz/shots/31150/)) and new interferometry on GOLEM (shots from [RetroActions](https://golem.fjfi.cvut.cz/RetroActions/1122Interferometry))

## Dirigent notebooks

Jupyter notebooks for phase detection
