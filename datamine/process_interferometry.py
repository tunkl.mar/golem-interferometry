#!/usr/bin/env python3
# -*- coding: utf-8 -*-
DEBUG = False

from tqdm.auto import tqdm, trange # progress bar
from tqdm.contrib.concurrent import process_map  # use multiprocessing for web scrapping
import requests
from bs4 import BeautifulSoup
import os

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd


from scipy import constants
from scipy import signal as sigproc
from scipy.fft import next_fast_len
import math
import isfreader

import warnings
warnings.filterwarnings("ignore", category=np.VisibleDeprecationWarning) 

from dataclasses import dataclass
import datetime

dprint = print if DEBUG else lambda *args: None
old_shot_no_threshold = 31150



ds = np.DataSource('data')   # sotarge for downloaded files
density_ULR = 'http://golem.fjfi.cvut.cz/shots/{shot_no}/Diagnostics/Interferometry/ne_lav.csv'
raw_data_URL = 'http://golem.fjfi.cvut.cz/shots/{shot_no}/Diagnostics/Interferometry/DAS_raw_data_dir/ch{ch_id}.isf'
scalars_URL = 'http://golem.fjfi.cvut.cz/shots/{shot_no}/Diagnostics/BasicDiagnostics/Results/{name}'
meta_URL = 'http://golem.fjfi.cvut.cz/shots/{shot_no}/{name}'
pressure_URL = 'http://golem.fjfi.cvut.cz/shots/{shot_no}/Devices/RASPs/Chamber/{name}'


old_basis_URL = "http://golem.fjfi.cvut.cz/shots/{shot_no}/analysis/Basics/0411ShotHomepage.ONN/{name}"
old_raw_data_URL = 'http://golem.fjfi.cvut.cz/shots/{shot_no}/DAS/0311TektronixDPO3014.ON/tektronix3014.npz'
old_utils_URL = "http://golem.fjfi.cvut.cz/utils/data/{shot_no}/{name}" # hope it's working
old_density_URL = "http://golem.fjfi.cvut.cz/shots/{shot_no}/diagnostics/Microwaves/0712Interferometer.ON/results/electron_density_line.npz"
old_shot_nums_file = 'data/oldIterferometryShotNums.npy'


# Load shot numbers with Interferomety data
def get_retroActions_shot_nums():
    URL = "https://golem.fjfi.cvut.cz/RetroActions/1122Interferometry"
    page = requests.get(URL)
    soup = BeautifulSoup(page.content, 'html.parser')
    a_tags = soup.find_all('a')
    shot_nums = [int(a_tag.text) for a_tag in a_tags]

    # seems the first 10 shots have the same ne_lav.csv
    shot_nums = shot_nums[10:]
    return shot_nums


def is_valid_URL(url):
    return requests.head(url).status_code < 400


def has_interferometry_data(shot_no:int):
    return is_valid_URL(old_density_URL.format(shot_no = shot_no))

def get_old_shot_nums():
    # load list from file if exists
    if not os.path.exists(old_shot_nums_file):
        shot_nums = np.arange(20120, old_shot_no_threshold+1)
        has_data = process_map(has_interferometry_data, shot_nums,max_workers=4, chunksize = 5)
        has_data = np.array(has_data)

        shot_nums = shot_nums[has_data]
        np.save(old_shot_nums_file, shot_nums)
    else:
        shot_nums = np.load(old_shot_nums_file)
    
    
    return shot_nums


def load_channel(shot_no, chanel_id, channel_name=None, in_ms=True, redownload = False):
    url = raw_data_URL.format(shot_no=shot_no, ch_id=chanel_id)
    if redownload and os.path.exists(ds.abspath(url)):
        dprint(f'removing already existing file for {shot_no}')
        os.remove(ds.abspath(url))
    fname = ds.open(url).name
    try:
        data = isfreader.read_file(fname)
    except:
        return None
    
    if data.dtype != np.float_:
        return None
    signal = pd.Series(data[:,1], name=channel_name,
                       index=pd.Index(data[:,0], name='time'))
    if in_ms:
        signal.index *= 1e3  # s -> ms
    return signal


def get_scalar(shot_no : int, name : str, type = float, URL = scalars_URL):
    return type(ds.open(URL.format(shot_no=shot_no, name=name)).read())

def get_plasma_times(shot_no: int):
    if shot_no <= old_shot_no_threshold:
        is_plasma = get_scalar(shot_no, 'Plasma', bool, URL=old_basis_URL)
        t_plasma_start = get_scalar(shot_no, 'PlasmaStart', URL=old_basis_URL)*1e3
        t_plasma_end = get_scalar(shot_no, 'PlasmaEnd', URL=old_basis_URL)*1e3
        return is_plasma, t_plasma_start, t_plasma_end
    else:
        is_plasma = get_scalar(shot_no, 'is_plasma', bool)
        t_plasma_start = get_scalar(shot_no, 't_plasma_start')
        t_plasma_end = get_scalar(shot_no, 't_plasma_end')
        return is_plasma, t_plasma_start, t_plasma_end

def get_shot_params(shot_no: int):
    if shot_no <= old_shot_no_threshold:
        U_loop_mean = get_scalar(shot_no, 'UloopMean', URL = old_basis_URL)
        I_pl_mean = get_scalar(shot_no, 'IplaMean', URL = old_basis_URL)
        B_tor_mean = get_scalar(shot_no, 'BtMean', URL = old_basis_URL)
        pressure_pre_dis = get_scalar(shot_no, 'Aktual_PfeifferMerkaVakua', URL = old_basis_URL)
    else:
        U_loop_mean = get_scalar(shot_no, 'U_loop_mean')
        I_pl_mean = get_scalar(shot_no, 'Ip_mean')
        B_tor_mean = get_scalar(shot_no, 'Bt_mean')
        pressure_pre_dis = get_scalar(shot_no, 'p_chamber_pressure_predischarge', URL=pressure_URL)
    return U_loop_mean, I_pl_mean, B_tor_mean, pressure_pre_dis
    
def get_session_id(shot_no : int):
    if shot_no <= old_shot_no_threshold:
        ## this returns session name (was there session id for old shots?) 
        ## if causes problems try to convert to hash % 100000000
        return get_scalar(shot_no, 'session_name', str, old_utils_URL) 
    else:
        return get_scalar(shot_no, 'session_id', int, meta_URL)
        

def get_shot_datetime(shot_no : int):
    if shot_no <= old_shot_no_threshold:
        ## DK where its stored 
        ## basicdiagn/shot_date
        ##            shot_time
        ## do not exists :/
        ## strip from web?
        return None

    else:
        shot_date  = get_scalar(shot_no,'shot_date', str, meta_URL).replace("\n", "")
        shot_time  = get_scalar(shot_no,'shot_time', str, meta_URL).replace("\n", "")        
        return datetime.datetime.strptime(shot_date+ " " +shot_time, "%y-%m-%d %H:%M:%S" )

        
def load_density_data(shot_no: int):
    '''load plasma density from csv/npz'''
    if shot_no <= old_shot_no_threshold:
        data =  ds.open(old_density_URL.format(shot_no=shot_no))
        npz_file = np.load(data.name, allow_pickle=True)
    
        ## from http://golem.fjfi.cvut.cz/shots/20234/diagnostics/Microwaves/0712Interferometer.ON/main.py
        ## and http://golem.fjfi.cvut.cz/wiki/_showraw/SW/pygolem/pygolem_lite.py
        ## it seems that time vector is not (usually) saved 
        ## should be extrapolated from t_star, t_end and len(data)
        if 'tvec' not in npz_file:
            tvec = np.linspace(npz_file['t_start'], npz_file['t_end'], len(npz_file['data']))
        else:
            tvec = npz_file['tvec']
        
        ## for convenience convert time to ms
        if tvec.max() < 2.:
            tvec*=1000.
        density = pd.Series(npz_file['data'], name='density', 
                            index=pd.Index(tvec, name='time'))
        return density  
    else:
        
        csv =  ds.open(density_ULR.format(shot_no=shot_no))
        return pd.read_csv(csv, names = ['time', 'density'], index_col='time').squeeze()

def load_density_raw(shot_no, redownload = False):

    if shot_no <= old_shot_no_threshold:
        ## force temporary datasource
        temp_ds = np.DataSource(None)
        dprint(f"downloading raw density for {shot_no}")
        ulr = old_raw_data_URL.format(shot_no=shot_no)
        #if redownload and os.path.exists(ds.abspath(ulr)):
        #    print(f'removing already existing file for {shot_no}')
        #    os.remove(ds.abspath(ulr))
            
        fname = temp_ds.open(ulr).name
        npz_file = np.load(fname, allow_pickle=True)
        
        
        time = npz_file['tvec']*1000
        diode_out = pd.Series(npz_file['data'][:,0], name='interfdiodeoutput',
                            index=pd.Index(time, name='time'))
        
        ref_saw = pd.Series(npz_file['data'][:,2], name='interframpsign',
                            index=pd.Index(time, name='time'))
        
        del temp_ds
    
    else:
        diode_out = load_channel(shot_no, 1, 'diode_output', redownload= redownload)
        ref_saw = load_channel(shot_no, 3, 'sawtooh_reference', redownload= redownload)

    return ref_saw, diode_out

def load_corrected_density(shot_no: int):
    density_fix_fname = 'ne_%i_v2.pkl' % shot_no
    density_no_fix_fname = 'ne_%i_v2_nf.pkl' % shot_no
    number_of_bad_peaks_fname = 'no_b_p_%i_v2.npy'  % shot_no
    if not ds.exists(density_fix_fname) or \
            not ds.exists(density_no_fix_fname)  or \
            not ds.exists(density_fix_fname):
        return None, None, None
    else:
        density_file= ds.abspath(density_fix_fname)
        density_no_fix_file= ds.abspath(density_no_fix_fname)
        no_fixes_file = ds.abspath(number_of_bad_peaks_fname)
        
        density_fix   = pd.read_pickle(density_file)
        density_no_fix = pd.read_pickle(density_no_fix_file)
        n_no_fix = np.load(no_fixes_file)
        
        return density_fix, density_no_fix, n_no_fix
    
def save_corrected_density(shot_no :int, 
                           density : pd.Series, 
                           density_no_fix : pd.Series, 
                           number_of_bad_peaks: int):
    density_fix_fname = 'ne_%i_v2.pkl' % shot_no
    density_nofix_fname = 'ne_%i_v2_nf.pkl' % shot_no
    density.to_pickle(ds._destpath + "/" + density_fix_fname)
    density_no_fix.to_pickle(ds._destpath + "/" + density_nofix_fname)
    
    
    number_of_bad_peaks_path = ds._destpath + "/" +  'no_b_p_%i_v2.npy' % shot_no
    np.save(number_of_bad_peaks_path, np.array(number_of_bad_peaks))
    
@dataclass
class InterferometryShot:
    '''Dataclass for interferometry and metadata'''
    shot_no: int
    is_plasma : bool
    plasma_start : float
    plasma_end : float
    has_interferometry_data : bool
    has_complete_interferometry: bool
    analog_interferometer : pd.DataFrame  = None
    digital_interferometer_original : pd.DataFrame = None
    digital_interferometer_new_fixed : pd.DataFrame = None
    digital_interferometer_new_no_fix : pd.DataFrame = None
    digital_interferometer_fixed_vars : list = None
    new_number_of_bad_peaks : int = 0
    sawtooh_reference : pd.DataFrame = None
    diode_output : pd.DataFrame = None
    session_id : int = None
    shot_time : datetime = None
    U_loop_mean : float = None
    I_plasma_mean : float = None
    B_tor_mean : float = None
    pressure_before_discharge : float = None
        
    def compute_corrected_density(self, recompute = False, redownload = False):
        self.digital_interferometer_new_fixed, \
        self.digital_interferometer_new_no_fix, \
        self.new_number_of_bad_peaks = load_corrected_density(self.shot_no)
        
        
        if self.digital_interferometer_new_fixed  is None or recompute:
            try:
                    
                self.sawtooh_reference, self.diode_output = load_density_raw(self.shot_no, redownload)
                
                self.digital_interferometer_new_fixed, \
                self.digital_interferometer_new_no_fix, \
                self.new_number_of_bad_peaks,  \
                self.digital_interferometer_fixed_vars = get_corrected_density(self)
                
            except Exception as err:    
                dprint(f"{err} can't (re)compute density-fix for {self.shot_no}")
            
                self.digital_interferometer_new_fixed   = None
                self.digital_interferometer_new_no_fix = None
                self.new_number_of_bad_peaks    = -1
                
            # save at least zero array placeholder
            if  self.digital_interferometer_new_fixed is None:
                self.digital_interferometer_new_fixed  = pd.Series(dtype=float)
                self.digital_interferometer_new_no_fix = pd.Series(dtype=float)
                self.new_number_of_bad_peaks    = -1
            
            save_corrected_density(self.shot_no, 
                                    self.digital_interferometer_new_fixed, 
                                    self.digital_interferometer_new_no_fix, 
                                    self.new_number_of_bad_peaks)
            
        ## if its only zero len Series return None
        if self.digital_interferometer_new_fixed.size == 0:
            self.digital_interferometer_new_fixed = None
        if self.digital_interferometer_new_no_fix.size == 0:
            self.digital_interferometer_new_no_fix = None
            
        if self.sawtooh_reference is not None and self.is_plasma and  \
                ( self.sawtooh_reference.index[-1] < self.plasma_start or self.sawtooh_reference.index[0] > self.plasma_end ):
            self.has_complete_interferometry = False
            
        if self.digital_interferometer_new_fixed is not None and self.is_plasma and  \
                ( self.digital_interferometer_new_fixed.index[-1] < self.plasma_start or self.digital_interferometer_new_fixed.index[0] > self.plasma_end ):
            self.has_complete_interferometry = False

omega_carrier = 2*np.pi*71e9  # 71 GHz microwave generator
a = 0.085   # limiter radius
L = 2*a  # central plasma chord estimate
prop_const = constants.elementary_charge**2 /  \
             (2*omega_carrier* constants.speed_of_light*constants.epsilon_0*constants.m_e)

def get_shot_data(shot_no : int):
    is_all = True
    is_complete = True
    
    if shot_no <= old_shot_no_threshold:
        analog = None
    else:
        try:
            analog = load_channel(shot_no, 4, 'analog')
            analog *=  1./ prop_const / L
            analog = analog
        except IOError as err:
            print(f"{err} for {shot_no} in load_channel analog")
            analog = None 
            is_all = False
                
        
    try: 
        digital = load_density_data(shot_no)
    except IOError as err:
        dprint(f"{err} for {shot_no} in load_density_data")
        digital = None 
        is_all = False
    

    try: 
        is_plasma, t_start, t_end = get_plasma_times(shot_no)
    except IOError as err: 
        dprint(f"{err} for {shot_no} in get_plasma_times")
        is_plasma, t_start, t_end = None, None, None
        is_all = False
        
    try:
        U_loop, I_plasma, B_tor, pressure = get_shot_params(shot_no)
    except (IOError, ValueError) as err:
        dprint(f"{err} for {shot_no} in get_shot_params")
        U_loop, I_plasma, B_tor, pressure = [None] * 4
    
    
    if digital is not None and is_plasma and ( digital.index[-1] < t_end or digital.index[0] > t_start ):
        is_complete = False

        
    try:
        sesstion_id = get_session_id(shot_no)
    except Exception as err:
        dprint(f"{err} for {shot_no} in get_session_id")
        sesstion_id = None
    
    try:
        shot_datetime = get_shot_datetime(shot_no)
    except Exception as err:
        dprint(f"{err} for {shot_no} in get_shot_datetime")
        shot_datetime = None
    
    return InterferometryShot(shot_no = shot_no,
                              analog_interferometer = analog,
                              digital_interferometer_original = digital,
                              is_plasma = is_plasma,
                              plasma_start = t_start,
                              plasma_end = t_end,
                              has_interferometry_data = is_all,
                              has_complete_interferometry=is_complete,
                              session_id = sesstion_id,
                              shot_time = shot_datetime,
                              U_loop_mean=U_loop,
                              I_plasma_mean=I_plasma,
                              B_tor_mean=B_tor,
                              pressure_before_discharge=pressure)

# %% [markdown]
# ### Download data
# 
# ! this will download about **37 GB** !

def download_shot_data(shot_no: int):
    try:
        _ = get_shot_data(shot_no)
        _ = load_density_raw(shot_no)
        return True
    except :
        return False
#has_data = process_map(download_shot_data, shot_nums,max_workers=2, chunksize = 1)
#md('All data downloaded') if np.all(has_data) else md('something failed')

# %%

## tests for interferomerty validation

def ends_with_zero(data, threshold = 1e18, start = 1, end = 0, use_abs = True, test_func = np.all):
    '''Check if data are below threshold in selected time slice (with respect to data endpoint)'''
    if start < end:
        raise ValueError("Must be start > end")
    data = np.abs(data) if use_abs  else data
    return test_func(data[(data.index[-1]-start):(data.index[-1]-end)] < threshold)

def is_zero_in_slice(data, threshold = 1e18, start = 1, end = 2, use_abs = True, test_func = np.all):
    '''Check if data are below threshold in selected time slice'''
    if start > end:
        raise ValueError("Must be start < end")
    if start < data.index[0]:
        start = data.index[0]
    end = end if end < data.index[-1] else data.index[-1]
    data = np.abs(data) if use_abs  else data
    return test_func(data[start:end] < threshold)

def starts_at_zero(data, threshold = 1e18, start = 0, end = 1, use_abs = True, test_func = np.all):
    if start > end:
        raise ValueError("Must be start < end")
    data = np.abs(data) if use_abs else data
    return test_func(data[(data.index[0]+start):(data.index[0]+end)] < threshold)


def is_valid_interferometry(inter_data: pd.Series, shot_data: InterferometryShot, 
                            zer_thr : int = 5, pos_thr: int = 75):
    
    # return None on no data
    if inter_data is None:
        return None, None, None, None
        
    # do not check shot witouth plasma
    if not shot_data.is_plasma:
        is_valid, positive, e_zero, negative_for_plasma = True, None, None, None
        return is_valid, positive, e_zero, negative_for_plasma
    
    #
    # ends at 'zero' (at least zer_thr of maximum)
    threshold = inter_data.max() * zer_thr /100
    threshold = np.max([5e16,threshold]) 
    
    s_start = shot_data.plasma_end + 1.  # 1   ms after plasma end
    s_end = inter_data.index[-1]   -.5   #  .5 ms before data ends 
                                         #  (data  sometimes ends with small peak)
                                         #  can be fixed by smoothing??
        
    # or check at least .5 ms of shot``
    if s_start > s_end: 
        s_start = inter_data.index[-1] - 1
    
    ends_near_zero = is_zero_in_slice(inter_data, threshold=threshold, 
                              start = s_start, end = s_end)
    
    #
    # is has positive values if there is a plasma
    plasma_slice  = inter_data[shot_data.plasma_start : shot_data.plasma_end]
    if plasma_slice.size == 0:
        # bad plasma detection?
        is_valid, positive, ends_near_zero, negative_for_plasma = None, None, None, None
    else:
        
        positive = np.count_nonzero(plasma_slice > 1e16) / plasma_slice.size* 1e2
        
        # has more than pos_thr% of positive values during plasma
        is_positive = True if positive > pos_thr else False
        
        # but this is not enough
        # discard all shots that go below zero with density 
        
        s_start = shot_data.plasma_start + 2  #  ms
        s_end = shot_data.plasma_end     - 2  #  ms
        
        if s_start > s_end: 
            negative_for_plasma = False    
        else:
            negative_for_plasma = is_zero_in_slice(inter_data, threshold=0., start=s_start, end=s_end, 
                                                   use_abs=False, test_func=np.any)
        
        is_valid = ends_near_zero and is_positive and not negative_for_plasma
        
    return is_valid, positive, ends_near_zero, negative_for_plasma


def pd_series_no_ndarray(sr):
    return np.array((sr.index, sr)).T

def get_corrected_density(shot_data: InterferometryShot):
    
    ## %% [markdown]
    # ### Data loading
    # unwrap dataclass

    ## %%
    t_plasma_start = shot_data.plasma_start
    t_plasma_end = shot_data.plasma_end
    is_plasma =  shot_data.is_plasma

    if shot_data.diode_output is None or shot_data.sawtooh_reference is None:
        return None, None, None, None
    mixer = pd_series_no_ndarray(shot_data.diode_output)
    ref_saw = pd_series_no_ndarray(shot_data.sawtooh_reference)

    ###########################################################
    ###########################################################
    ## copy from dirigent-ntb


    ## %%
    x, y = mixer.shape
    f_s = x / (mixer[-1, 0] - mixer[0, 0])  # ms -> kHz
    #print('Sampling frequency is {} MHz.'.format(round(f_s / 1000)))

    ## %% [markdown]
    # ### Spectral analysis of the signal<br>
    # 
    # The mixer signal is a base sine wave (the envelope of the mixing) at a frequency close to 500 kHz. The reference saw-tooth frequency sweeping wave has the same base frequency, but with a trail of harmonics forming the sharp saw-tooth shape.

    ## %%
    def calculate_spectrum(signal, target_dfreq=10):
        nperseg = int(f_s / target_dfreq)
        f, psd = sigproc.welch(signal[:, 1], fs=f_s, nperseg=nperseg, nfft=next_fast_len(nperseg))
        return f, psd

    ## %%
    ref_saw_f, ref_saw_psd = calculate_spectrum(ref_saw)
    mixer_f, mixer_psd = calculate_spectrum(mixer)
    f_base = mixer_f[mixer_psd.argmax()]
    f_base_ref_saw = ref_saw_f[ref_saw_psd.argmax()]
    #print('The base frequency of the mixer is {} kHz.'.format(f_base))
    #print('The base frequency of the ref_saw is {} kHz.'.format(f_base_ref_saw))

    ## %%
    # fig, ax = plt.subplots(dpi=150)
    # ax.set(xlabel='frequency [kHz]', ylabel='power spectral density [V$^2$]')
    # ax.plot(ref_saw_f, ref_saw_psd, label='ref_saw')
    # ax.plot(mixer_f, mixer_psd, label='mixer')
    # plt.axvline(f_base, label=f'base frequency f={f_base:.0f} kHz', color='C3')
    # ax.loglog()
    # plt.grid()
    # plt.legend();

    ## %% [markdown]
    # ### Extract the baseband from the signal<br>
    # 
    # The instantaneous phase and amplitude of the base signal can be inferred only for the baseband, i.e. by removing higher and lower frequencies (i.e. extracting the base sine wave from the reference saw-tooth signal).

    ## %%
    base_band_hwidth = 50  # kHz
    base_band_filter = sigproc.iirfilter(8, [f_base - base_band_hwidth, f_base + base_band_hwidth], fs=f_s, btype='bandpass', output='sos')

    ## %%
    def freq_filter(signal, sos_filter):
        signal[:, 1] = sigproc.sosfiltfilt(sos_filter, signal[:, 1])
        return signal

    ## %%
    mixer_filtered = freq_filter(mixer, base_band_filter)
    ref_saw_filtered = freq_filter(ref_saw, base_band_filter)

    ## %% [markdown]
    # cut 0.1 ms from the beginning and from the end for better signal processing

    ## %%
    mixer_filtered = mixer_filtered[(mixer_filtered[:, 0] < (mixer_filtered[-1, 0] - 0.1)) & ((mixer_filtered[0, 0] + 0.1) < mixer_filtered[:, 0])]
    ref_saw_filtered = ref_saw_filtered[(ref_saw_filtered[:, 0] < (ref_saw_filtered[-1, 0] - 0.1)) & ((ref_saw_filtered[0, 0] + 0.1) < ref_saw_filtered[:, 0])]

    ## %% [markdown]
    # ### Define signal processing functions<br>
    # 
    # func find_peaks - finds peaks with optimal output array

    ## %%
    def find_peaks(data):
        peaks_indexes, _ = sigproc.find_peaks(data[:, 1])
        return np.vstack((data[peaks_indexes, 0], data[peaks_indexes, 1])).T

    ## %% [markdown]
    # func initial_phase_shift - mixer and ref_saw signals are in general a bit phase shifted from each other -> it calculates "initial" phase shift and removes it

    ## %%
    def initial_phase_shift(peaks, peaks_ref):
        phase_mean = peaks[0, 0] - peaks_ref[0, 0]
        peaks_ref[:, 0] += phase_mean
        return peaks_ref

    ## %% [markdown]
    # func cut_edges - cut first and last data point, which is distorted from the spectral filter

    ## %%
    def cut_edges(peaks, peaks_ref):
        peaks = peaks[(peaks[0, 0] < peaks[:, 0]) & (peaks[:, 0] < peaks[-1, 0])]
        peaks_ref = peaks_ref[(peaks_ref[0, 0] < peaks_ref[:, 0]) & (peaks_ref[:, 0] < peaks_ref[-1, 0])]
        return peaks, peaks_ref

    ## %% [markdown]
    # func smooth - classic func for signal smoothing

    ## %%
    def smooth(y, box_pts):
        box = np.ones(box_pts) / box_pts
        y_smooth = np.convolve(y, box, mode='same')
        return y_smooth

    ## %% [markdown]
    # func without_correction - a sum of basic operations

    ## %%
    def without_correction(mixer_filtered, ref_saw_filtered):
        peaks = find_peaks(mixer_filtered)
        peaks_ref = find_peaks(ref_saw_filtered)
        peaks_ref = initial_phase_shift(peaks, peaks_ref)
        peaks, peaks_ref = cut_edges(peaks, peaks_ref)
        return peaks, peaks_ref

    ## %% [markdown]
    # func find_nearest - finds the nearest peak of the given one

    ## %%
    def find_nearest(array, value):
        idx = (np.abs(array[:, 0] - value)).argmin()
        return array[idx].copy()

    ## %% [markdown]
    # func calc_dphase_unchanged - calculates dphase from unrepaired data

    ## %%
    def calc_dphase_unchanged(peaks, peaks_ref):
        x_peaks, y_peaks = peaks.shape
        x_ref_peaks, y_ref_peaks = peaks_ref.shape
        dphase = np.ones((min(x_peaks, x_ref_peaks), 2))
        for i in range(0, int(len(dphase))):
            dphase[i, 0] = peaks[i, 0]
            dphase[i, 1] = peaks[i, 0] - peaks_ref[i, 0]
        return dphase

    ## %% [markdown]
    # func calc_lost_phase - calculates lost phase in the signal - to define how much the signal was damaged

    ## %%
    def calc_lost_phase(peaks, peaks_ref):
        dphase = calc_dphase_unchanged(peaks, peaks_ref)
        time_interval = 0.1  # ms
        indexes = np.argwhere(dphase[:, 0] > (dphase[-1, 0] - time_interval))
        data = dphase[indexes[:, 0]]
        return 2 * math.pi * f_base * np.average(data[:, 1]), dphase

    ## %% [markdown]
    # func optimizing_cycle - defines the most probable parts of the data, where the signal was damaged and deletes the corresponding waveforms from the reference signal, because these waveforms did not travelled properly through the plasma

    ## %%
    def optimizing_cycle(number_of_bad_peaks, deriv_sort, distance, peaks, peaks_ref):
        bad_peaks_indexes = np.empty((0, 1))
        k = 0  # help variable
        l = 0  # help variable
        while k < number_of_bad_peaks and l < len(peaks[:, 0]):
            index = np.argwhere(peaks[:, 0] == deriv_sort[l, 0])
            if (len(bad_peaks_indexes) != 0 and (abs((index[0, 0] - find_nearest(bad_peaks_indexes, index[0, 0]))) < distance)) or (t_plasma_end < peaks[index, 0]) or (peaks[index, 0] < t_plasma_start):
                l += 1
            else:
                bad_peaks_indexes = np.vstack((bad_peaks_indexes, index[0, 0]))
                peaks_ref = np.delete(peaks_ref, index, 0)
                k += 1
                l += 1
        return bad_peaks_indexes, peaks_ref

    ## %% [markdown]
    # func repair - creates the most plausible repair of the given damaged interferometer data (probably caused by the scatter of the probing wave from the plasma, plasma instability, ...)

    ## %%
    def repair(lost_phase, dphase, peaks, peaks_ref):
        # make repairing cycle
        number_of_bad_peaks = round(lost_phase / (2 * math.pi))
        if number_of_bad_peaks >= 1:
            smooth_factors = np.zeros((0, 1))
            distances = np.zeros((0, 1))
            varieties = np.zeros((0, 1))
            dphase_all_variants = list()
            for smooth_factor in range(1, 50):
                deriv = dphase.copy()
                deriv[:, 1] = np.gradient(dphase[:, 1])
                deriv[:, 1] = smooth((deriv[:, 1]), smooth_factor)
                deriv_sort = deriv.copy()
                deriv_sort = deriv_sort[deriv_sort[:, 1].argsort()[::-1]]
                for distance in range(1, 50):
                    bad_peaks_indexes, repaired_ref_peaks = optimizing_cycle(number_of_bad_peaks, deriv_sort, distance, peaks, peaks_ref)
                    dphase_final = calc_dphase(peaks, repaired_ref_peaks)
                    dphase_final[:, 1] = smooth(dphase_final[:, 1], 100)
                    smooth_factors = np.vstack((smooth_factors, smooth_factor))
                    distances = np.vstack((distances, distance))
                    varieties = np.vstack((varieties, calc_curve_length(dphase_final)))
                    dphase_all_variants.append(dphase_final.copy())
            all = np.hstack((smooth_factors, distances, varieties))
            varieties_min = varieties.argmin()
            best_smooth_factor = int(all[int(varieties_min), 0])
            best_distance = int(all[int(varieties_min), 1])
        else:
            best_smooth_factor = 10
            best_distance = 10
            dphase_all_variants = list()
        deriv = dphase.copy()
        deriv[:, 1] = np.gradient(dphase[:, 1])
        deriv[:, 1] = smooth((deriv[:, 1]), best_smooth_factor)
        deriv_sort = deriv.copy()
        deriv_sort = deriv_sort[deriv_sort[:, 1].argsort()[::-1]]
        bad_peaks_indexes, repaired_ref_peaks = optimizing_cycle(number_of_bad_peaks, deriv_sort, best_distance, peaks, peaks_ref)
        dphase_final = calc_dphase(peaks, repaired_ref_peaks)
        dphase_final[:, 1] = smooth(dphase_final[:, 1], 100)
        
        if len(dphase_all_variants) == 0:
            dphase_all_variants = [dphase_final.copy()]
        return dphase_final, number_of_bad_peaks, dphase_all_variants

    ## %% [markdown]
    # func calc_dphase - calculates dphase

    ## %%
    def calc_dphase(mixer_peaks, repaired_ref_peaks):
        x_peaks, y_peaks = mixer_peaks.shape
        x_ref_peaks, y_ref_peaks = repaired_ref_peaks.shape
        x_dphase = min(x_peaks, x_ref_peaks)
        dphase_final = np.ones((x_dphase, 2))
        dphase_final[:, 0] = repaired_ref_peaks[:x_dphase, 0]
        dphase_final[:, 1] = mixer_peaks[:x_dphase, 0] - repaired_ref_peaks[:x_dphase, 0]
        dphase_final[:, 1] *= 2 * math.pi * f_base
        return dphase_final

        
    ## %% [markdown]
    # func calc_curve_length - calculates the length of the dphase curve as a key parameter to decide, whether the repair is good enough

    ## %%
    def calc_curve_length(dphase_final):
        length = np.sum( np.sqrt(np.diff(dphase_final[:, 0])**2 + np.diff(dphase_final[:,1]) **2 ))
        return length


    ## %% [markdown]
    # ### Final signal processing cycle

    ## %%
    # prepare data (without final data correction)
    peaks_no_corr, peaks_ref_no_corr = without_correction(mixer_filtered, ref_saw_filtered)
    # calculate lost phase
    lost_phase, dphase_zero = calc_lost_phase(peaks_no_corr, peaks_ref_no_corr)
    # make final data repair
    dphase_final, number_of_bad_peaks, dphase_all_vars = repair(lost_phase, dphase_zero, peaks_no_corr, peaks_ref_no_corr)

    ## %% [markdown]
    # ### Estimation of the electron density<br>
    # 
    # The ordinary wave (O-mode) with a carrier frequency $\omega$ traveling through a collisionless plasma with the plasma frequency $\omega_{p} = \sqrt{\frac{n_e e^2}{\epsilon_0 m_e}}$ has a refractive index $$N_O=\sqrt{1-\frac{\omega_p^2}{\omega^2}}$$
    # Under the assumption that the carrier wave frequency is much larger than the plasma frequency $\omega>>\omega_p$ this formula can be expanded into a Taylor series as $$N_O\approx 1-\frac{\omega_p^2}{2\omega^2}$$
    # A wave traveling through a medium with a refractive index $N(l)$ accumulates a total phase shift $\varphi = \frac{\omega}{c} \int N(l) \mathrm{d}l$. Therefore, in comparison to a wave traveling in vacuum (or clear air) with $N\approx 1$, the  wave traveling through the plasma over a chord with length $L$ accumulates a relative phase shift of $$\Delta \varphi = \frac{e^2}{2\omega c\epsilon_0 m_e}\int\limits_L n_e(l) \mathrm{d}l$$
    # Therefore, it is possible to estimate the line-averaged density $\bar n_e = \frac{1}{L} \int\limits_L n_e(l) \mathrm{d}l$ from the detected phase shift between the reference and mixer signals.

    ## %%
    omega_carrier = 2 * np.pi * 71e9  # 71 GHz microwave generator
    a = 0.085  # limiter radius [m]
    L = 2 * a  # central plasma chord estimate
    prop_const = constants.elementary_charge ** 2 / (2 * omega_carrier * constants.speed_of_light * constants.epsilon_0 * constants.m_e)

    ## %%
    ne_lav = dphase_final.copy()
    ne_lav[:, 1] = ne_lav[:, 1] * (1 / (prop_const * L))
#    ne_lav = ne_lav[(ne_lav[:, 0] >= 0) & (ne_lav[:, 0] <= (t_plasma_end + 5))]

    for i in range(len(dphase_all_vars)):
        dphase_all_vars[i][:,1] = dphase_all_vars[i][:,1] * (1 / (prop_const * L))

    ## %%
    dphase_zero[:, 1] *= 2 * math.pi * f_base
    dphase_zero[:, 1] = smooth(dphase_zero[:, 1], 100)

    ## %%
    ne_lav_zero = dphase_zero.copy()
    ne_lav_zero[:, 1] = ne_lav_zero[:, 1] * (1 / (prop_const * L))
#    ne_lav_zero = ne_lav_zero[(ne_lav_zero[:, 0] >= 0) & (ne_lav_zero[:, 0] <= (t_plasma_end + 5))]


    ## %% [markdown]
    # ### Pack output
   
    ne_lav = pd.Series(data = ne_lav[:,1], name='density_repaired',
                       index=pd.Index(ne_lav[:,0], name='time'))
    
    ne_lav_no_fix = pd.Series(data = ne_lav_zero[:,1], name='density_damaged',
                       index=pd.Index(ne_lav_zero[:,0], name='time'))
    
    ne_lav_vars = [pd.Series(data = ne_lav_var[:,1], name='density_rep_var%d' % i,
                             index=pd.Index(ne_lav_var[:,0], name='time')) 
                   for i, ne_lav_var in enumerate(dphase_all_vars)]
    
    return ne_lav, ne_lav_no_fix, number_of_bad_peaks, ne_lav_vars
  



def process_shot_worker(shot_no):
    shot_data = get_shot_data(shot_no)
    if not shot_data.has_interferometry_data or not shot_data.is_plasma:
        return None    
    shot_data.compute_corrected_density(recompute=True)

    analog           = is_valid_interferometry(shot_data.analog_interferometer, shot_data)
    digital_original = is_valid_interferometry(shot_data.digital_interferometer_original, shot_data)
    digital_fixed    = is_valid_interferometry(shot_data.digital_interferometer_new_fixed, shot_data)
    digital_no_fix   = is_valid_interferometry(shot_data.digital_interferometer_new_no_fix, shot_data)

    return (shot_no, shot_data.shot_time, shot_data.session_id, shot_data.is_plasma, 
            shot_data.U_loop_mean, shot_data.I_plasma_mean, shot_data.B_tor_mean, shot_data.pressure_before_discharge,
            shot_data.has_complete_interferometry,
            *analog, *digital_original, *digital_fixed, *digital_no_fix,
            shot_data.new_number_of_bad_peaks)


def run_for_shots(shot_nums):

    data = [process_shot_worker(s) for s in tqdm(shot_nums, leave = True, position = 0, desc = 'processing shots')]
    data = process_map(process_shot_worker, shot_nums, max_workers = 8, chunksize = 1,
                          leave = True, position = 0, desc = 'processing shots')
    
    # remove None values
    data = [i for i in data if i is not None]

    df = pd.DataFrame(data, columns = ("shot_no", "time", "session", "is_plasma",
                                       "U_loop", "I_pl", "B_tor", "p_pre_discharge", 
                                       "has_complete_interferometry_data",
                                       "density_analog_valid", "positive_analog", "zero_analog", "negative_in_plasma_analog",
                                       "density_original_valid", "positive_original", "zero_original", "negative_in_plasma_original",
                                       "density_fix_valid", "positive_fix", "zero_fix",  "negative_in_plasma_fix",
                                       "density_no_fix_valid", "positive_no_fix", "zero_no_fix", "negative_in_plasma_no_fix",
                                       "n_bad_peaks"))
    df = df.set_index('shot_no')
    return df

def run_for_retroActions_shots():
    shot_nums = get_retroActions_shot_nums()
    df = run_for_shots(shot_nums)
    
    df.to_csv('InterferometryDF.csv')
    df.to_pickle('InterferometryDF.pkl')

def run_for_old_shots():
    shot_nums = get_old_shot_nums()[:5000]
    
    df = run_for_shots(shot_nums)
    
    df.to_csv('OldInterferometryDF.csv')
    df.to_pickle('OldInterferometryDF.pkl')


def download_old_shot_data():
    shot_nums = get_old_shot_nums()
    
    has_data = process_map(download_shot_data, shot_nums,  max_workers = 4, chunksize = 1, leave = True, position = 0, desc = 'downloading shots')
    print('All data downloaded') if np.all(has_data) else print('something failed')
    
if __name__ == "__main__":
    run_for_retroActions_shots()
    #download_old_shot_data()
    
    #run_for_old_shots()
    
    
    
    
